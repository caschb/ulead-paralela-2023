import asyncio
import aiofiles


async def leer_archivo_seccion(archivo, inicio, fin):
    async with aiofiles.open(archivo, mode="rb") as file:
        await file.seek(inicio)
        data = await file.read(fin - inicio)
        return data


async def main():
    archivo = "archivo_grande.txt"
    tamano_archivo = await obtener_tamano_archivo(archivo)

    # Define el número de secciones y el tamaño de cada sección
    num_secciones = 4
    tamano_seccion = tamano_archivo // num_secciones

    # Lista para almacenar los datos leídos de cada sección
    secciones = []

    # Lee cada sección de forma concurrente
    tasks = []
    for i in range(num_secciones):
        inicio = i * tamano_seccion
        fin = inicio + tamano_seccion if i < num_secciones - 1 else tamano_archivo
        task = asyncio.create_task(leer_archivo_seccion(archivo, inicio, fin))
        tasks.append(task)

    await asyncio.gather(*tasks)

    # Obtén los datos de cada tarea
    for task in tasks:
        seccion_data = await task
        secciones.append(seccion_data)

    # Procesa los datos de las secciones aquí


async def obtener_tamano_archivo(archivo):
    async with aiofiles.open(archivo, mode="rb") as file:
        await file.seek(0, 2)  # Mover el puntero al final del archivo
        tamano = await file.tell()
        return tamano


if __name__ == "__main__":
    asyncio.run(main())
