import multiprocessing

def tarea_proceso(queue):
    for i in range(5):
        queue.put(f'Mensaje {i}')

if __name__ == '__main__':
    cola = multiprocessing.Queue()

    proceso = multiprocessing.Process(target=tarea_proceso, args=(cola,))
    proceso.start()
    proceso.join()

    while not cola.empty():
        mensaje = cola.get()
        print(f'Recibido: {mensaje}')
