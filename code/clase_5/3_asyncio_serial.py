import time

def saludar(nombre):
    print(f'Hola, {nombre}!')
    time.sleep(1)
    print(f'Adios, {nombre}!')

def main():
    saludar('Alice')
    saludar('Bob')
    saludar('Charlie')

main()
