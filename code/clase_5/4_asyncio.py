import asyncio
import aiohttp

async def obtener_pagina(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.text()

async def main():
    url = 'https://www.google.com'
    contenido = await obtener_pagina(url)
    print(f'Longitud de la página: {len(contenido)} caracteres')

asyncio.run(main())
