import asyncio

async def saludar(nombre):
    print(f'Hola, {nombre}!')
    await asyncio.sleep(1)
    print(f'Adios, {nombre}!')

async def main():
    await asyncio.gather(
        saludar('Alice'),
        saludar('Bob'),
        saludar('Charlie')
    )

asyncio.run(main())
