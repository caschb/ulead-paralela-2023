import multiprocessing
import time
import random

# Tamaño máximo del búfer
BUFFER_SIZE = 5

# Lista para el búfer
buffer = multiprocessing.Queue(BUFFER_SIZE)

# Función para el productor
def productor():
    while True:
        item = random.randint(1, 100)
        print(f"Productor produce: {item}")
        if not buffer.full():
            buffer.put(item)
        time.sleep(random.uniform(0.1, 0.5))

# Función para el consumidor
def consumidor():
    while True:
        if not buffer.empty():
            item = buffer.get()
            print(f"Consumidor consume: {item}")
        time.sleep(random.uniform(0.1, 0.5))

# Crear procesos para el productor y el consumidor
productor_process = multiprocessing.Process(target=productor)
consumidor_process = multiprocessing.Process(target=consumidor)

# Iniciar los procesos
productor_process.start()
consumidor_process.start()

# Esperar a que ambos procesos terminen (esto no sucederá ya que los procesos se ejecutan indefinidamente)
productor_process.join()
consumidor_process.join()
