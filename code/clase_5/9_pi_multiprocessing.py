import multiprocessing
import random
import time
from sys import argv

# Número de puntos a generar
total_puntos = int(argv[1])

# Contador para puntos dentro del círculo
dentro_circulo = multiprocessing.Value('i', 0)

# Función para calcular si un punto (x, y) está dentro del círculo unitario
def calcular_puntos():
    local_dentro_circulo = 0
    for _ in range(total_puntos):
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)
        distancia_al_origen = x**2 + y**2
        if distancia_al_origen <= 1:
            local_dentro_circulo += 1
    with dentro_circulo.get_lock():
        dentro_circulo.value += local_dentro_circulo

# Crear una lista de procesos
num_procesos = 4  # Puedes ajustar este número según la cantidad de núcleos de tu CPU
procesos = []

start = time.time()
# Crear procesos y dividir el trabajo
for _ in range(num_procesos):
    proceso = multiprocessing.Process(target=calcular_puntos)
    procesos.append(proceso)

# Iniciar los procesos
for proceso in procesos:
    proceso.start()

# Esperar a que todos los procesos terminen
for proceso in procesos:
    proceso.join()
total = time.time() - start

# Calcular el valor de π
pi_estimado = (dentro_circulo.value / (total_puntos * num_procesos)) * 4
print(f'Valor estimado de π (serial): {pi_estimado}, tiempo = {total} s')
