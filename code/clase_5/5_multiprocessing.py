import multiprocessing

def imprimir_numeros():
    for i in range(1, 6):
        print(f'Número: {i}')

def imprimir_letras():
    for letra in 'ABCDE':
        print(f'Letra: {letra}')

if __name__ == '__main__':
    proceso1 = multiprocessing.Process(target=imprimir_numeros)
    proceso2 = multiprocessing.Process(target=imprimir_letras)

    proceso1.start()
    proceso2.start()

    proceso1.join()
    proceso2.join()

    print('Ambos procesos han terminado.')
