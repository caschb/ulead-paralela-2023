import numpy as np
import time
import os
from sys import argv

def matrix_multiplication(A, B):
    C = np.matmul(A, B)
    print(C)

def main():
    # Define the dimensions of the matrices
    start_time = time.time()

    matrix_size = int(argv[1])
    np.random.seed(0)
    # Initialize two matrices A and B
    matrix_A = np.random.rand(matrix_size, matrix_size)
    matrix_B = np.random.rand(matrix_size, matrix_size)
    matrix_multiplication(matrix_A, matrix_B)

    total_time = time.time() - start_time
    output_file_name = argv[2]
    exists = os.path.exists(output_file_name)
    if exists:
        with open(output_file_name, 'a') as f:
            f.write(f"{1},{matrix_size},{total_time},numpy\n")
    else:
        with open(output_file_name, 'w') as f:
            f.write("1,matrix_size,total_time,type\n")
            f.write(f"{1},{matrix_size},{total_time},numpy\n")

if __name__ == "__main__":
    main()