from mpi4py import MPI
import numpy as np
import time
import os
from sys import argv

def distribute_matrix(matrix, comm, root=0):
    rows, cols = matrix.shape
    local_rows = rows // comm.Get_size()
    local_matrix = np.empty((local_rows, cols), dtype=matrix.dtype)

    comm.Scatter(matrix, local_matrix, root=root)

    return local_matrix

def distributed_matrix_multiplication(A, B):
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    # Scatter matrix A and broadcast matrix B
    local_A = distribute_matrix(A, comm, root=0)
    B = comm.bcast(B, root=0)

    # Initialize the result matrix
    local_rows_A, cols_A = local_A.shape
    cols_B = B.shape[1]
    local_C = np.zeros((local_rows_A, cols_B), dtype=float)

    # Perform local matrix multiplication
    for i in range(local_rows_A):
        for j in range(cols_B):
            for k in range(cols_A):
                local_C[i][j] += local_A[i][k] * B[k][j]


    # Gather results from all processes
    result = np.zeros((A.shape[0], B.shape[1]), dtype=float)
    comm.Gather(local_C, result, root=0)

    if rank == 0:
        # Process 0 collects results from other processes and prints the result
        print(result)

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    comm_size = comm.Get_size()
    matrix_size = int(argv[1])

    if rank == 0:
        start_time = time.time()
        np.random.seed(0)
        A = np.random.rand(matrix_size, matrix_size)
        B = np.random.rand(matrix_size, matrix_size)

    else:
        A = np.empty((matrix_size, matrix_size))
        B = np.empty((matrix_size, matrix_size))
    distributed_matrix_multiplication(A, B)

    if rank == 0:
        total_time = time.time() - start_time
        output_file_name = argv[2]
        exists = os.path.exists(output_file_name)
        if exists:
            with open(output_file_name, 'a') as f:
                f.write(f"{comm_size},{matrix_size},{total_time},mpi\n")
        else:
            with open(output_file_name, 'w') as f:
                f.write("comm_size,matrix_size,total_time,type\n")
                f.write(f"{comm_size},{matrix_size},{total_time},mpi\n")

if __name__ == "__main__":
    main()
