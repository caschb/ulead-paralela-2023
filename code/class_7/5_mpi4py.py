from mpi4py import MPI

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    if rank == 0:
        # Process 0 sends a message asynchronously
        message = "Hello from Process 0!"
        request = comm.isend(message, dest=1, tag=0)

        # Do some work while waiting for the message to be received
        for i in range(1000000):
            pass

        # Check if the message has been sent
        if request.Test():
            print("Message sent from Process 0.")

    elif rank == 1:
        # Process 1 receives the message asynchronously
        request = comm.irecv(source=0, tag=0)

        # Do some work while waiting for the message to be received
        for i in range(1000000):
            pass
        # Check if the message has been received
        if request.Test():
            message = request.wait()
            print(f"Received: {message} (Process 1)")

if __name__ == "__main__":
    main()
