import argparse
import numpy as np


def saxpy(s, x, y):
    y = s * x + y
    return y


parser = argparse.ArgumentParser(
    prog='Map',
    description='Shows an example of map operation'
)
parser.add_argument('n', help='Size of the arrays to test',
                    type=int, default=100)

args = parser.parse_args()
size = args.n

x = np.arange(start=0, stop=size, dtype=int)
scalar = 10
y = np.arange(start=200, stop=200 + size, dtype=int)

print(y[-1])
y = saxpy(scalar, x, y)
print(y[-1])
