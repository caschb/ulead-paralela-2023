from mpi4py import MPI
from sys import argv
import logging
import random
import time

def compute_points(total_points):
  inside_circle = 0
  for _ in range(total_points):
    x = random.uniform(0, 1)
    y = random.uniform(0, 1)
    distance = x ** 2 + y ** 2
    if distance <= 1:
      inside_circle += 1
  return inside_circle

if __name__ == '__main__':
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  size = comm.Get_size()
  name = MPI.Get_processor_name()
  logging.basicConfig(level=logging.INFO, format=f'[%(filename)s:%(lineno)d on rank {rank}@{name} @ %(asctime)s]: %(message)s')

  total_points = int(argv[1])
  my_points = total_points // size
  start = time.time()
  inside_circle = compute_points(my_points)
  total = time.time() - start

  total_inside_circle = comm.reduce(inside_circle, op=MPI.SUM, root=0)

  if rank == 0:
    pi_estimate = (total_inside_circle / total_points) * 4
    logging.info(f'π is approximately: {pi_estimate}, time: {total}')