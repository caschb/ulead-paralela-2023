from mpi4py import MPI
from sys import argv
import logging

if __name__ == '__main__':
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  size = comm.Get_size()
  name = MPI.Get_processor_name()
  max_value = int(argv[1])

  logging.basicConfig(level=logging.INFO, format=f'[%(filename)s:%(lineno)d on rank {rank}@{name} @ %(asctime)s]: %(message)s')

  hops = 0
  while hops < max_value:
    if rank == 0:
      logging.info(hops)
      hops += 1
      comm.send(hops, dest=1)
      hops = comm.recv(source=1)
    else:
      hops = comm.recv(source=0)
      logging.info(hops)
      hops += 1
      comm.send(hops, dest=0)
