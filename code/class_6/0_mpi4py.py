from mpi4py import MPI
import logging

def print_hello(rank, size, name):
  msg = f"Hello world!, I am process {rank} of {size} on {name}"
  logging.info(msg)

if __name__ == '__main__':
  rank = MPI.COMM_WORLD.Get_rank()
  size = MPI.COMM_WORLD.Get_size()
  name = MPI.Get_processor_name()

  logging.basicConfig(level=logging.INFO, format=f'[%(filename)s:%(lineno)d on {name} @ %(asctime)s]: %(message)s')
  print_hello(rank, size, name)