import threading


contador = 0
lock = threading.Lock()
b = threading.Barrier(1)

def incrementar_contador():
    global contador
    for _ in range(1000000):
        # with lock:
        contador += 1

# Crear dos hilos
thread1 = threading.Thread(target=incrementar_contador)
thread2 = threading.Thread(target=incrementar_contador)

# Iniciar los hilos
thread1.start()
thread2.start()

# Esperar a que ambos hilos terminen
thread1.join()
thread2.join()

b.wait()
print(f'El valor final del contador es: {contador}')
