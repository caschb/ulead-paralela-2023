import random
import time
from sys import argv

# Número de puntos a generar
total_puntos = int(argv[1])

# Contador para puntos dentro del círculo
dentro_circulo = 0

# Función para calcular si un punto (x, y) está dentro del círculo unitario
def calcular_puntos():
    global dentro_circulo
    for _ in range(total_puntos):
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)
        distancia_al_origen = x**2 + y**2
        if distancia_al_origen <= 1:
            dentro_circulo += 1

start = time.time()
# Calcular el valor de π de forma serial
calcular_puntos()
total = time.time() - start

# Calcular el valor de π
pi_estimado = (dentro_circulo / total_puntos) * 4
print(f'Valor estimado de π (serial): {pi_estimado}, tiempo = {total} s')