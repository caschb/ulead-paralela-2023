import threading
import time
import random

# Tamaño máximo del búfer
BUFFER_SIZE = 5

# Lista para el búfer
buffer = []

# Función para el productor
def productor():
    while True:
        item = random.randint(1, 100)
        print(f"Productor produce: {item}")
        if len(buffer) < BUFFER_SIZE:
            buffer.append(item)
        time.sleep(random.uniform(0.1, 0.5))

# Función para el consumidor
def consumidor():
    while True:
        if buffer:
            item = buffer.pop(0)
            print(f"Consumidor consume: {item}")
        time.sleep(random.uniform(0.1, 0.5))

# Crear hilos para el productor y el consumidor
productor_thread = threading.Thread(target=productor)
consumidor_thread = threading.Thread(target=consumidor)

# Iniciar los hilos
productor_thread.start()
consumidor_thread.start()

# Esperar a que ambos hilos terminen (esto no sucederá ya que los hilos se ejecutan indefinidamente)
productor_thread.join()
consumidor_thread.join()
