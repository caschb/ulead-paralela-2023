import threading
import logging
from time import sleep
from random import randint

def imprimir_numeros():
    for i in range(1, 6):
        logging.info(f'Número: {i}')
        sleep(randint(1, 3))

def imprimir_letras():
    for letra in 'ABCDE':
        logging.info(f'Letra: {letra}')
        sleep(randint(1, 3))

if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")

    # Crear dos hilos
    thread1 = threading.Thread(target=imprimir_numeros)
    thread2 = threading.Thread(target=imprimir_letras)

    # Iniciar los hilos
    thread1.start()
    thread2.start()

    # Esperar a que ambos hilos terminen
    thread1.join()
    thread2.join()

    logging.info('Ambos hilos han terminado.')
