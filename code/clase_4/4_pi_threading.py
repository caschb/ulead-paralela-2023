import threading
import random
import time
from sys import argv

# Número de puntos a generar
total_puntos = int(argv[1])

# Contadores para puntos dentro y fuera del círculo
dentro_circulo = 0

# Función para calcular si un punto (x, y) está dentro del círculo unitario
def calcular_puntos():
    global dentro_circulo
    for _ in range(total_puntos):
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)
        distancia_al_origen = x**2 + y**2
        if distancia_al_origen <= 1:
            dentro_circulo += 1

# Crear una lista de hilos
num_hilos = 4  # Puedes ajustar este número según la cantidad de núcleos de tu CPU
hilos = []

start = time.time()
# Crear hilos y dividir el trabajo
for _ in range(num_hilos):
    hilo = threading.Thread(target=calcular_puntos)
    hilos.append(hilo)

# Iniciar los hilos
for hilo in hilos:
    hilo.start()

# Esperar a que todos los hilos terminen
for hilo in hilos:
    hilo.join()
total = time.time() - start

# Calcular el valor de π
pi_estimado = (dentro_circulo / total_puntos) * 4
print(f'Valor estimado de π (serial): {pi_estimado}, tiempo = {total} s')