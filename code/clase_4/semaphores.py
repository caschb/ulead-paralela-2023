import threading

sem = threading.Semaphore(2)  # Permite hasta 2 hilos a la vez

# Función que simula el acceso a un recurso
def acceder_recurso(id_hilo):
    with sem:
        print(f'Hilo {id_hilo} accediendo al recurso')
        # Realizar alguna operación en el recurso
        print(f'Hilo {id_hilo} liberando el recurso')

# Crear varios hilos que intentan acceder al recurso
hilos = []
for i in range(5):
    hilo = threading.Thread(target=acceder_recurso, args=(i,))
    hilos.append(hilo)

# Iniciar los hilos
for hilo in hilos:
    hilo.start()

# Esperar a que todos los hilos terminen
for hilo in hilos:
    hilo.join()

print('Fin de la ejecución')