import argparse


def saxpy(s, x, y):
    for i in range(len(y)):
        y[i] = s * x[i] + y[i]


parser = argparse.ArgumentParser(
    prog='Map',
    description='Shows an example of map operation'
)
parser.add_argument('n', help='Size of the arrays to test',
                    type=int, default=100)

args = parser.parse_args()
size = args.n

x = [i for i in range(0, size)]
scalar = 10
y = [i for i in range(200, 200 + size)]

print(y[-1])
saxpy(scalar, x, y)
print(y[-1])
