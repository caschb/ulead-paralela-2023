from PIL import Image
import argparse
import numpy as np
from math import pi, sqrt
from os import path


def apply_kernel(kernel, image: Image, horizontal=True) -> Image:
    r, g, b = image.split()
    half_point = len(kernel) // 2
    xdim, ydim = image.size
    output = Image.new(image.mode, image.size)
    for i in range(xdim):
        for j in range(ydim):
            addition = [0, 0, 0]
            for n in range(-half_point, half_point):
                if horizontal:
                    if 0 <= i + n < xdim:
                        kernel_pos = kernel[n + half_point]
                        addition[0] += kernel_pos * r.getpixel((i + n, j))
                        addition[1] += kernel_pos * g.getpixel((i + n, j))
                        addition[2] += kernel_pos * b.getpixel((i + n, j))
                else:
                    if 0 <= j + n < ydim:
                        kernel_pos = kernel[n + half_point]
                        addition[0] += kernel_pos * r.getpixel((i, j + n))
                        addition[1] += kernel_pos * g.getpixel((i, j + n))
                        addition[2] += kernel_pos * b.getpixel((i, j + n))
            output.putpixel((i, j), tuple(int(idx) for idx in addition))
    return output


def gaussian_1d_filter(sigma):
    filter_size = int(2 * pi * (sigma ** 2))
    filter = np.array(range(-filter_size//2, filter_size//2))
    factor = 1 / (sqrt(2 * pi) * sigma)
    filter = factor * np.exp(-(filter ** 2)/(2 * (sigma ** 2)))
    return filter


parser = argparse.ArgumentParser(
    prog='Stencil',
    description='Shows an example of an stencil operation'
)
parser.add_argument('filename', help='Filename of the picture to blur')
parser.add_argument('-r', '--radius',
                    help='Radius of the gaussian blur', type=int, default=1)

args = parser.parse_args()
filename = args.filename
radius = args.radius

im = Image.open(filename)

kernel = gaussian_1d_filter(radius)

partial = apply_kernel(kernel, im, horizontal=True)
out = apply_kernel(kernel, partial, horizontal=False)
out.save(f"out_r{radius}_{path.basename(filename)}")
