from line_profiler import profile
import math

@profile
def is_prime(n):
    if n == 1:
        return False
    if n == 2:
        return True
    for i in range(3, math.isqrt(n), 2):
        if n % i == 0:
            return False
    return True

def compute_primes_sum(limit):
    total_sum = 0
    for num in range(2, limit):
        if is_prime(num):
            total_sum += num
    return total_sum

if __name__ == "__main__":
    limit = 1000  # El rango de números primos a calcular
    result = compute_primes_sum(limit)
    print(f"La suma de los números primos hasta {limit} es: {result}")