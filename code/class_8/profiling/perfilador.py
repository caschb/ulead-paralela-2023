import programa_ejercicio2
import cProfile
import pstats

with cProfile.Profile() as pr:
  programa_ejercicio2.is_prime(100000)
pr.print_stats()
with cProfile.Profile() as pr:
  programa_ejercicio2.compute_primes_sum(100000)
pr.print_stats()