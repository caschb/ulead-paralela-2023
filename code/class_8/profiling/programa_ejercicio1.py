import numpy as np
from line_profiler import profile

@profile
def perform_calculations(data):
    result = 0
    for num in data:
        if num % 2 == 0:
            result += num * 2
        else:
            result += num * 3
    return result

@profile
def perform_calculations_fast(data):
    result = 0
    odds = data[data % 2 == 1]
    evens = data[data % 2 == 0]
    result += np.sum(odds * 3)
    result += np.sum(evens * 2)
    return result

if __name__ == "__main__":
    data = np.random.randint(1, high=100, size=10000000)
    total_result = perform_calculations(data)
    total_result2 = perform_calculations_fast(data)
    print(f"Resultado total: {total_result}")
    print(f"Resultado total: {total_result2}")

