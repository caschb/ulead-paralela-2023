from line_profiler import profile

@profile
def slow_function():
    total = 0
    for i in range(1000000):
        total += i
    return total

@profile
def fast_function():
    return sum(range(1000000))

@profile
def medium_function():
    list_nums = [i for i in range(1000000)]
    return sum(list_nums)

if __name__ == "__main__":
    slow_function()
    medium_function()
    fast_function()