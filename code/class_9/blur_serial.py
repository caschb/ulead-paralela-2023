from PIL import Image
import argparse
import numpy as np
from math import pi, sqrt
from os import path

def apply_kernel(kernel, image, horizontal=True):
    half_point = len(kernel) // 2
    xdim, ydim = image.shape
    output = np.zeros_like(image)
    for i in range(xdim):
        for j in range(ydim):
            addition = 0
            for n in range(-half_point, half_point):
                if horizontal:
                    if 0 <= i + n < xdim:
                        kernel_pos = kernel[n + half_point]
                        addition += kernel_pos * image[i + n, j]
                else:
                    if 0 <= j + n < ydim:
                        kernel_pos = kernel[n + half_point]
                        addition += kernel_pos * image[i, j + n]
            output[i, j] = addition
    return output

def gaussian_1d_filter(sigma):
    filter_size = int(2 * pi * (sigma ** 2))
    filter = np.arange(-filter_size//2, filter_size//2)
    factor = 1 / (sqrt(2 * pi) * sigma)
    filter = factor * np.exp(-(filter ** 2)/(2 * (sigma ** 2)))
    return filter


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
      prog='Stencil',
      description='Shows an example of an stencil operation'
  )
  parser.add_argument('filename', help='Filename of the picture to blur')
  parser.add_argument('-r', '--radius',
                      help='Radius of the gaussian blur', type=int, default=1)

  args = parser.parse_args()
  filename = args.filename
  radius = args.radius

  image = Image.open(filename).convert("L")
  image_arr = np.asarray(image)

  kernel = gaussian_1d_filter(radius)

  partial = apply_kernel(kernel, image_arr, horizontal=True)
  out = apply_kernel(kernel, partial, horizontal=False)
  outpic = Image.fromarray(out)
  outpic.save(f"out_r{radius}_{path.basename(filename)}")
